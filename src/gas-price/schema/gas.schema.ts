import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose"
import { Document } from "mongoose"

export type GasPriceDocument = GASPrice & Document

@Schema()
export class  GASPrice{
    @Prop()
    fast: number

    @Prop({required: true})
    slow: number

    @Prop()
    standard: number
    
    @Prop({default: Date.now})
    created_at: Date

    @Prop({default: Date.now})
    updated_at: Date
}

export const GASPriceSchema = SchemaFactory.createForClass(GASPrice);