import { Module } from '@nestjs/common';
import { GasPriceService } from './gas-price.service';
import { GasPriceController } from './gas-price.controller';
import { GASPrice, GASPriceSchema } from './schema/gas.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { GasPriceRepository } from './repository/GasPrice.repository';
import { GasPriceSchedule } from './schedule/GasPrice.schedule';

@Module({
  imports: [
    MongooseModule.forFeature([{
      name: GASPrice.name,
      schema: GASPriceSchema
    }
    ])
  ],
  providers: [
    GasPriceService,
    GasPriceRepository,
    GasPriceSchedule
  ],
  controllers: [
    GasPriceController
  ]
})
export class GasPriceModule { }
