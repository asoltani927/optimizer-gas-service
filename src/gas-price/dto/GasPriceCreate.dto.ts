export interface GasPriceCreateDTO{
    fast: number
    slow: number
    standard: number
}