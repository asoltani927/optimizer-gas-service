import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { GasPriceRepository } from '../repository/GasPrice.repository';
import { GasStationTool } from '../tools/gas.price.tool';

@Injectable()
export class GasPriceSchedule {
  constructor(private gas_repository: GasPriceRepository) {}

  @Cron('*/5 * * * * *', {
    name: 'gas_station_collector',
    // timeZone: process.env.TIMEZONE,
  })
  async trigger() {
    const data = await GasStationTool.get();
    if (data !== null) {
      const last = await this.gas_repository.findLast(1);
      if (
        !last[0] ||
        last[0].fast !== data.fast ||
        last[0].slow !== data.safeLow ||
        last[0].standard !== data.average
      ) {
        await this.gas_repository.create({
          fast: data.fast,
          slow: data.safeLow,
          standard: data.average,
        });
      }
    }
  }
}
