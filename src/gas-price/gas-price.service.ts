import { Injectable } from '@nestjs/common';
import { GasPriceRepository } from './repository/GasPrice.repository';

@Injectable()
export class GasPriceService {

    constructor(private repository : GasPriceRepository){

    }

    async last(){
        let data = await this.repository.findLast(1);
        return data[0];
    }
}
