import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model, UpdateWriteOpResult } from "mongoose";
import { GasPriceCreateDTO } from "../dto/GasPriceCreate.dto";
import { GASPrice, GasPriceDocument } from "../schema/gas.schema";

@Injectable()
export class GasPriceRepository {

    constructor(@InjectModel(GASPrice.name) private gasPriceModel: Model<GasPriceDocument>) { }

    async create(createGasPriceDTO: GasPriceCreateDTO): Promise<GASPrice> {
        let newCollection = new this.gasPriceModel(createGasPriceDTO)
        return await newCollection.save();
    }

    async findLast(count: number = 2): Promise<GASPrice[]> {
        return await this.gasPriceModel.find().sort({created_at:-1}).limit(count);
    }
}