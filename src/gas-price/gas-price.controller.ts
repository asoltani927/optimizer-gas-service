import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { GasPriceService } from './gas-price.service';

@Controller('gas-price')
export class GasPriceController {

    constructor(private readonly gas_service: GasPriceService) { }

    @MessagePattern('get_gas_station')
    async get_gas_station() {
        return this.gas_service.last();
    }

}
