import { Api } from "src/contracts/Api";

export class GasStationApi extends Api {

  public async get() {
    try {
      this.clear();
      this.setAction("/ethgasAPI.json");
      if (process.env.ETH_GAS_API_KEY)
        this.addParam("api-key", process.env.ETH_GAS_API_KEY);
      const response = await this.getResponse();
      if (response.status === 200) {
        // @ts-ignore
        return response.data;
      }
      return null;
    } catch (error) {
      return null
    }
  }
}

export const GasStationTool = new GasStationApi("https://ethgasstation.info/api")