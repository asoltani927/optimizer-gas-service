import { HttpService } from "@nestjs/axios";

export class Api {

    protected action: string;
    protected base: string;
    protected params: {};
    protected httpService: HttpService;

    constructor(base: string) {
        this.params = {}
        this.base = base;
        this.httpService = new HttpService();
    }

    getUrl() {
        // @ts-ignore
        if (this.params) {
            var query = new URLSearchParams();
            Object.keys(this.params).forEach((key) => {
                query.append(key, this.params[key])
            })
            return this.base + this.action + "?" + query.toString();
        }
        return this.base + this.action;
    }

    getResponse(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const url = this.getUrl();
                return await this.httpService.get(url, {
                    headers: {
                        "content-type": 'application/json'
                    },
                    timeout: 50000,
                }).subscribe((response) => {
                    resolve(response)
                }, (error) => {
                    try {
                        console.log(url + ' ' + error.response.status + ' ' + error.response.statusText)
                        reject(error)
                    } catch (err) {
                        console.log(err)
                    }
                })
            } catch (e) {
                throw e
            }
        })
    }

    protected addParam(name: string, value: any) {
        // @ts-ignore
        this.params[name] = value;
    }

    protected clear() {
        // @ts-ignore
        this.params = {};
    }

    protected setAction(path: string) {
        // @ts-ignore
        this.action = path;
    }

}