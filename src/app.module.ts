import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { MONGO_CONNECTION } from './app.properties';
import { ConfigModule } from '@nestjs/config';
import { GasPriceModule } from './gas-price/gas-price.module';

@Module({
  imports: [
    ConfigModule.forRoot(),

    GasPriceModule,

    HttpModule.register({
      timeout: 5000,
      maxRedirects: 3
    }),
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(MONGO_CONNECTION),
  ],
})
export class AppModule {}
